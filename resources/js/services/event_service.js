import {http,http_auth} from './http_service'

export function createEvent(data) {
    return http().post('/events',data);
}

export function createEventLineup(data) {
    return http().post('/eventlineup',data);
}

export function getEvents() {
    return http().get('/events');
}

export function getEventsFromId(id) {
    return http().get('/events/' + id);
}

export function createParticipant(data) {
    return http().post('/participants',data);
}


export function getParticipantsFromId(id) {
    return http().get('/participant/events/' + id);
}
