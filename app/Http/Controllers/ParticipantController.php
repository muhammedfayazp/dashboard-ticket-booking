<?php

namespace App\Http\Controllers;

use App\Event;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ParticipantController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store participant');
        $request->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
        ]);

        $participant = new Participant();
        $participant->name = $request->name;
        $participant->phone = $request->phone;
        $participant->email = $request->email;
        $participant->event_id = $request->event_id;
        $participant->tkt_type = $request->tkt_type;


        if ($participant->save()){
            return response()->json($participant,200);
        }else{
            return response()->json($participant,500);
        }
    }

    public function participantDetails(Request $request)
    {
        $result=DB::table('participants')->where('event_id','=',$request->id)
            ->rightJoin('events', 'participants.event_id', '=', 'events.id')
            ->select( 'participants.*','events.*')
            ->get();
        if(sizeof($result)==0){
            $result=DB::table('events')->where('id','=',$request->id)->get();
        }
        return response()->json($result,200);
    }

}
