<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventLineUp;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $events=Event::query()->orderBy('created_at')->paginate();
        return response()->json($events,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store event');
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'venue'=>'required',
            'start_date'=>'date|required',
            'end_date'=>'date|after_or_equal:start_date',
            'gold_tkt_capacity'=>'required',
            'silver_tkt_capacity'=>'required',
            'platinum_tkt_capacity'=>'required',
            'gold_tkt_rate'=>'required',
            'silver_tkt_rate'=>'required',
            'platinum_tkt_rate'=>'required'
        ]);

        $event = new Event();
        $event->title = $request->title;
        $event->description = $request->description;
        $event->venue = $request->venue;
        $event->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $event->start_time = Carbon::parse($request->start_time)->format('H:i:s');
        $event->end_date = Carbon::parse($request->end_date)->format('Y-m-d');
        $event->end_time = Carbon::parse($request->end_time)->format('H:i:s');
        $event->gold_tkt_capacity = $request->gold_tkt_capacity;
        $event->silver_tkt_capacity = $request->silver_tkt_capacity;
        $event->platinum_tkt_capacity = $request->platinum_tkt_capacity;
        $event->gold_tkt_rate = $request->gold_tkt_rate;
        $event->silver_tkt_rate = $request->silver_tkt_rate;
        $event->platinum_tkt_rate = $request->platinum_tkt_rate;

        if ($event->save()){
            return response()->json($event,200);
        }else
        {
            return response()->json($event,500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }


    public function eventDetails(Request $request)
    {
        $event=DB::table('events_lineup')->where('event_id','=',$request->id)
            ->rightJoin('events', 'events_lineup.event_id', '=', 'events.id')
            ->select( 'events_lineup.*','events.*')
            ->get();
        if(sizeof($event)==0){
            $event=DB::table('events')->where('id','=',$request->id)->get();
        }
        return response()->json($event,200);
    }

    public function createlineup(Request $request){
        Log::info('create event lineup');
        $eventLineup = new EventLineUp();
        $eventLineup->event_id=$request->event_id;
        $eventLineup->event_date = Carbon::parse($request->event_date)->format('Y-m-d');
        $eventLineup->event_time = Carbon::parse($request->event_time)->format('H:i:s');
        $eventLineup->event_title = $request->event_title;
        $eventLineup->event_description = $request->event_description;
        $eventLineup->save();
        return response()->json($eventLineup,200);
    }


}
