<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TicketController extends Controller
{
    public function index()
    {
        $events=DB::table('events')
            ->join('tickets','events.id','=','tickets.event_id')
            ->get();
        return response()->json($events,200);
    }

}
