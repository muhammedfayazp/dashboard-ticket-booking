<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{

    use SoftDeletes;

    protected $table = 'events';

    protected $fillable = ['title',
        'description',
        'venue',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'gold_tkt_capacity',
        'silver_tkt_capacity',
        'platinum_tkt_capacity',
        'gold_tkt_rate',
        'silver_tkt_rate',
        'platinum_tkt_rate'];


    public function getId()
    {
        return $this->id;
    }

}
