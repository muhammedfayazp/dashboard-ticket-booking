<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{
    use SoftDeletes;

    protected $table = 'participants';


    protected $fillable = [
        'name',
        'phone',
        'email',
        'event_id',
        'tkt_type',];
}
