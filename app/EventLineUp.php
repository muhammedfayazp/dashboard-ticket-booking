<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventLineUp extends Model
{
    use SoftDeletes;

    protected $table = 'events_lineup';

    protected $fillable = ['event_id','event_date','event_date','event_title','event_description'];

}
