<?php

use App\Http\Controllers\ParticipantController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'auth'],function (){
    Route::post('register','AuthController@register');
    Route::post('login','AuthController@login');

    Route::group(['middleware'=>'auth:api'],function (){
        Route::get('logout','AuthController@logout');
    });
});

Route::group(['middleware' => 'auth:api'],function () {
    Route::post('events', [EventsController::class, 'store']);
    Route::post('participants', [ParticipantController::class, 'store']);
    Route::post('eventlineup', [EventsController::class, 'createlineup']);
    Route::get('/events', [EventsController::class, 'index']);
    Route::get('/register-events', [\App\Http\Controllers\TicketController::class, 'index']);
    Route::get('/events/{id}', [EventsController::class, 'eventDetails']);
    Route::get('/participant/events/{id}', [ParticipantController::class, 'participantDetails']);
});
